# WALLET SERVICE

The goal was to develop a very simple project in order to support functionalities of a **WALLET**.
<br/>
To achieve this goal I have considered three main Entity:
1. Users: this model is implemented to help us in having **Authentication** logic, then when a user is registered, a default account would be created for the user. So User entity has oneToOne relationship with Account entity.
2. Account: each User can have only one Account where we will keep the **Balance** of each user there. 
3. Transaction: Users are allowed to **Credit** or **Debit**, to keep the track of each user's action on their Account this model is implemented. So this entity has ManyToOne relation with Account entity.

<br/>
This project implemented with the following stack:
<ul>
<li>JAVA, as the base language of project</li>
<li>SPRING-BOOT, as the main framework of the project</li>
<li>H2-DATABASE, lightweight database which needs no configuration, we used its ability to persist data on file and prevent loosing data after restart</li>
<li>JWT, is used to have secured APIs and to have Authorization and Authentication functionalities over them</li>
<li>HIBERNATE, an open source and lightweight ORM(Object Relational Mapping) tool that help us in interacting with database and persisting data</li>
<li>GRADLE, as the build tool and dependency manager for the project</li>
<li>JUNIT & SPRING-TEST & MOCKITO are also used in order to write integration tests for APIs and unit tests for service and repository layer</li>
<li>DOCKER, is used to run the project in a containerized environment</li>
<li>SWAGGER, is used to have an auto-generated API documentation</li>
</ul>

# HOW TO RUN PROJECT
**You just need to have `Docker` installed on your machine, then follow the steps below**

1. Clone the repository, in `/wallet-service` directory using command :
`git clone https://gitlab.com/sobhan_Ssh/wallet-service.git`

2. `cd wallet-service`

3. Build the project using project's wrapped gradle with executing command :
   `./gradlew build`  (you may face `permission denied error` then you need to execute the command `chmod +x gradlew` before the build command)

4. Build a docker image using below:
`docker build --build-arg JAR_FILE=build/libs/wallet-service-0.0.1.jar -t wallet .`

5. Run the pre-built image using below :
`docker run -d -p 2121:2121 --name=wallet-container wallet`


# INSTRUCTION
<br/>
Based on the project requirements and business logic I have implemented a few APIs which includes: 
<ul>
<li>APIs to have Register, Login and user-info functionalities</li>
<li>APIs to have Credit and Debit functionalities over user's balance</li>
<li>APIs to have check-balance and transactions-history functionalities over each account</li>
</ul>

 ******please consider that only Register and Login services are allowed to be called without providing token, and for other services you need to provide 'Authorization' az key and, the token which will be returned as the response of the LOGIN service, as the value, in request header, for example:*****

**`Authorization : Bearer 'JWT-TOKEN'`**
<br/>

- I have enabled `Swagger-ui` documentation for the project and after running it you can access it using the link below : [localhost:2121/swagger-ui.html]()
- There is also an export of API services for using in Postman application, this file exists in the root of project with name of **wallet-service.postman_collection.json**

- Also, I have enabled **H2 Database Console**, you can use it in your browser with following URL: [localhost:2121/h2-console]() , when project is running, needed parameters are :
1. Username: root
2. Password: 123456
3. Url: jdbc:h2:file:~/WALLET-DB
