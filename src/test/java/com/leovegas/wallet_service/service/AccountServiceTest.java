package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.request.CreditRequestDto;
import com.leovegas.wallet_service.dto.request.DebitRequestDto;
import com.leovegas.wallet_service.dto.response.BalanceResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.AccountRepository;
import com.leovegas.wallet_service.service.impl.AccountServiceImpl;
import com.leovegas.wallet_service.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private TransactionServiceImpl transactionService;

    @Test
    public void should_create_account_for_user() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account expected = new Account(new BigDecimal("0"), savedUser, 0);
        when(accountRepository.save(any(Account.class))).thenReturn(expected);

        // When
        Account actual = accountService.createAccountForUser(savedUser);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(accountRepository, times(1)).save(any(Account.class));
        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void should_debit_an_account_and_return_new_balance() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);
        final DebitRequestDto debitRequestDto = new DebitRequestDto(new BigDecimal("2500"), UUID.randomUUID().toString());
        debitRequestDto.setUsername("sobhan");
        final BalanceResponseDto expected = new BalanceResponseDto(savedAccount.getBalance().subtract(debitRequestDto.getAmount()));
        final Account updatedAccount = new Account(savedAccount.getBalance().subtract(debitRequestDto.getAmount()), savedUser, 0);
        when(accountRepository.findAccountByUser_Username(any(String.class))).thenReturn(Optional.of(savedAccount));
        when(accountRepository.save(any(Account.class))).thenReturn(updatedAccount);

        // When
        BalanceResponseDto actual = accountService.debitAccount(debitRequestDto);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(accountRepository, times(1)).findAccountByUser_Username(any(String.class));
        verify(accountRepository, times(1)).save(any(Account.class));
        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void should_credit_an_account_and_return_new_balance() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);
        final CreditRequestDto creditRequestDto = new CreditRequestDto(new BigDecimal("2500"), UUID.randomUUID().toString());
        creditRequestDto.setUsername("sobhan");
        final BalanceResponseDto expected = new BalanceResponseDto(savedAccount.getBalance().add(creditRequestDto.getAmount()));
        final Account updatedAccount = new Account(savedAccount.getBalance().add(creditRequestDto.getAmount()), savedUser, 0);
        when(accountRepository.findAccountByUser_Username(any(String.class))).thenReturn(Optional.of(savedAccount));
        when(accountRepository.save(any(Account.class))).thenReturn(updatedAccount);

        // When
        BalanceResponseDto actual = accountService.creditAccount(creditRequestDto);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(accountRepository, times(1)).findAccountByUser_Username(any(String.class));
        verify(accountRepository, times(1)).save(any(Account.class));
        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void should_return_the_account_balance() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);
        final BalanceResponseDto expected = new BalanceResponseDto(savedAccount.getBalance());
        when(accountRepository.findAccountByUser_Username(any(String.class))).thenReturn(Optional.of(savedAccount));

        // When
        BalanceResponseDto actual = accountService.getBalance(savedAccount.getUser().getUsername());

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(accountRepository, times(1)).findAccountByUser_Username(any(String.class));

        verifyNoMoreInteractions(accountRepository);
    }
}
