package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.security.JWTUtil;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.UserRepository;
import com.leovegas.wallet_service.service.impl.AccountServiceImpl;
import com.leovegas.wallet_service.service.impl.AuthenticationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private AccountServiceImpl accountService;
    @Mock
    private AuthenticationManager authManager;
    @Mock
    private JWTUtil jwtUtil;

    @Test
    public void should_register_user_and_return_user_detail() {
        // Given
        final RegisterUserRequestDto userRequestDto = new RegisterUserRequestDto("sobhan","123456");
        final UserResponseDto expected = new UserResponseDto(userRequestDto.getUsername(), Role.ROLE_USER.name());
        when(userRepository.save(any(User.class))).thenReturn(new User(userRequestDto.getUsername(), userRequestDto.getPassword(), Role.ROLE_USER));

        // When
        final UserResponseDto actual = authenticationService.register(userRequestDto);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(userRepository, times(1)).save(any(User.class));
        verify(userRepository, times(1)).existsUserByUsername(any(String.class));
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void should_login_user_and_return_token() {
        // Given
        final LoginRequestDto loginRequestDto = new LoginRequestDto("sobhan", "123456");
        final TokenResponseDto expectedResponse = new TokenResponseDto("A_TOKEN_FOR_USER");
        when(jwtUtil.generateToken(any(String.class))).thenReturn("A_TOKEN_FOR_USER");

        // When
        final TokenResponseDto actual = authenticationService.login(loginRequestDto);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedResponse);
    }
}
