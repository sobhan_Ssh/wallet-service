package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.Transaction;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.enums.TransactionType;
import com.leovegas.wallet_service.repository.TransactionRepository;
import com.leovegas.wallet_service.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Mock
    private TransactionRepository transactionRepository;

    @Test
    public void should_create_debit_transaction_for_account() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);
        final BigDecimal transactionAmount = new BigDecimal("2000");
        final TransactionType transactionType = TransactionType.DEBIT;
        final String transactionId = UUID.randomUUID().toString();
        final Transaction expected = new Transaction(transactionAmount, transactionId, transactionType, savedAccount);
        when(transactionRepository.save(any(Transaction.class))).thenReturn(new Transaction(transactionAmount, transactionId, transactionType, savedAccount));
        when(transactionRepository.existsByTransactionId(any(String.class))).thenReturn(false);

        // When
        Transaction actual = transactionService.createTransaction(savedAccount, transactionAmount, transactionId, transactionType);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(transactionRepository, times(1)).save(any(Transaction.class));
        verify(transactionRepository, times(1)).existsByTransactionId(any(String.class));
        verifyNoMoreInteractions(transactionRepository);
    }

    @Test
    public void should_create_credit_transaction_for_account() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);
        final BigDecimal transactionAmount = new BigDecimal("2000");
        final TransactionType transactionType = TransactionType.CREDIT;
        final String transactionId = UUID.randomUUID().toString();
        final Transaction expected = new Transaction(transactionAmount, transactionId, transactionType, savedAccount);
        when(transactionRepository.save(any(Transaction.class))).thenReturn(new Transaction(transactionAmount, transactionId, transactionType, savedAccount));
        when(transactionRepository.existsByTransactionId(any(String.class))).thenReturn(false);

        // When
        Transaction actual = transactionService.createTransaction(savedAccount, transactionAmount, transactionId, transactionType);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(transactionRepository, times(1)).save(any(Transaction.class));
        verify(transactionRepository, times(1)).existsByTransactionId(any(String.class));
        verifyNoMoreInteractions(transactionRepository);
    }

    @Test
    public void should_get_accounts_transaction_of_an_account() {
        // Given
        final User savedUser = new User("sobhan", "123456", Role.ROLE_USER);
        final Account savedAccount = new Account(new BigDecimal("5000"), savedUser, 0);

        final Transaction transaction1 = new Transaction(new BigDecimal("2500"), UUID.randomUUID().toString(), TransactionType.CREDIT, savedAccount);
        final Transaction transaction2 = new Transaction(new BigDecimal("1500"), UUID.randomUUID().toString(), TransactionType.DEBIT, savedAccount);
        List<Transaction> transactions = Arrays.asList(transaction1, transaction2);

        final TransactionResponseDto transactionResponse1 = new TransactionResponseDto(transaction1.getTransactionId(), transaction1.getType().name(), transaction1.getAmount(), null);
        final TransactionResponseDto transactionResponse2 = new TransactionResponseDto(transaction2.getTransactionId(), transaction2.getType().name(), transaction2.getAmount(), null);
        final ListResponseDto<TransactionResponseDto> expected = new ListResponseDto<>(0, 2, Arrays.asList(transactionResponse1, transactionResponse2));

        Page<Transaction> pagedResponse = new PageImpl<>(transactions);
        when(transactionRepository.findTransactionsByAccount_Id(any(), any())).thenReturn(Optional.of(pagedResponse));

        // When
        ListResponseDto<TransactionResponseDto> actual = transactionService.getAccountTransactions(savedAccount, 0, 10);

        // Then - Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(transactionRepository, times(1)).findTransactionsByAccount_Id(any(), any());
        verifyNoMoreInteractions(transactionRepository);
    }
}
