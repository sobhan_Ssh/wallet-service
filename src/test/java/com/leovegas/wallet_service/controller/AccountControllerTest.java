package com.leovegas.wallet_service.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.leovegas.wallet_service.dto.request.CreditRequestDto;
import com.leovegas.wallet_service.dto.request.DebitRequestDto;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.*;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.AccountRepository;
import com.leovegas.wallet_service.repository.TransactionRepository;
import com.leovegas.wallet_service.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author: s.shakeri
 * at 9/16/2022
 **/

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    @BeforeEach
    @AfterEach
    void setup() {
        transactionRepository.deleteAll();
        accountRepository.deleteAll();
        userRepository.deleteAll();
    }

    private UserResponseDto performMockRegisterUserAPICall(RegisterUserRequestDto registerUserRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerUserRequestDto)));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(registerUserRequestDto.getUsername())))
                .andExpect(jsonPath("$.role", is(Role.ROLE_USER.name())));

        // map response to dto and return
        MvcResult registerMvcResult = response.andReturn();
        String registerContentAsString = registerMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(registerContentAsString, UserResponseDto.class);
    }

    private TokenResponseDto performMockLoginAPICall(LoginRequestDto loginRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto)));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.token", is(notNullValue())));

        // map response to dto and return
        MvcResult loginMvcResult = response.andReturn();
        String loginContentAsString = loginMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(loginContentAsString, TokenResponseDto.class);
    }

    private BalanceResponseDto performMockCreditAccountAPICall(String jwtToken, CreditRequestDto creditRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(put("/api/account/credit")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken)
                .content(objectMapper.writeValueAsString(creditRequestDto)));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance", is(notNullValue())));

        // map response to dto and return
        MvcResult creditMvcResult = response.andReturn();
        String creditContentAsString = creditMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(creditContentAsString, BalanceResponseDto.class);
    }

    private BalanceResponseDto performMockDebitAccountAPICall(String jwtToken, DebitRequestDto debitRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(put("/api/account/debit")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken)
                .content(objectMapper.writeValueAsString(debitRequestDto)));

        // validate response
        response
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.balance", is(notNullValue())));

        // map response to dto and return
        MvcResult debitMvcResult = response.andReturn();
        String debitContentAsString = debitMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(debitContentAsString, BalanceResponseDto.class);
    }

    private BalanceResponseDto performMockGetAccountBalanceAPICall(String jwtToken) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(get("/api/account/balance")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken));

        // validate response
        response
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.balance", is(notNullValue())));

        // map response to dto and return
        MvcResult balanceMvcResult = response.andReturn();
        String balanceContentAsString = balanceMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(balanceContentAsString, BalanceResponseDto.class);
    }

    private ListResponseDto<TransactionResponseDto> performMockGetAccountTransactionsHistoryAPICall(String jwtToken) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(get("/api/account/history")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken));

        // validate response
        response
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data", is(notNullValue())))
                .andExpect(jsonPath("$.page", is(notNullValue())))
                .andExpect(jsonPath("$.size", is(notNullValue())));

        // map response to dto and return
        MvcResult historyMvcResult = response.andReturn();
        String historyContentAsString = historyMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(historyContentAsString, ListResponseDto.class);
    }

    @Test
    public void scenario_registerUser_thenLoginWithRegisteredUser_thenGetJwtToken_thenCreditUsersAccount() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(userResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /register API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // step 5 - create CreditRequestDto
        CreditRequestDto creditRequestDto = new CreditRequestDto(new BigDecimal("567.89"), UUID.randomUUID().toString());

        // step 6 - call /api/account/credit API and get new account balance
        BalanceResponseDto balanceResponseDto = performMockCreditAccountAPICall(tokenResponseDto.getToken(), creditRequestDto);

        // then - verify the result content using assert statements
        assertEquals(
                balanceResponseDto.getBalance().stripTrailingZeros(),
                creditRequestDto.getAmount().stripTrailingZeros()
        );
    }

    @Test
    public void scenario_registerUser_thenLoginWithRegisteredUser_thenGetJwtToken_thenCreditUsersAccount_thenDebitUsersAccount() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(userResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /register API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // step 5 - create CreditRequestDto
        CreditRequestDto creditRequestDto = new CreditRequestDto(new BigDecimal("567.89"), UUID.randomUUID().toString());

        // step 6 - call /api/account/credit API and get new account balance
        BalanceResponseDto balanceAfterCreditResponseDto = performMockCreditAccountAPICall(tokenResponseDto.getToken(), creditRequestDto);

        // step 7 - create DebitRequestDto
        DebitRequestDto debitRequestDto = new DebitRequestDto(new BigDecimal("123.45"), UUID.randomUUID().toString());

        // step 8 - call /api/account/debit API and get new account balance
        BalanceResponseDto balanceAfterDebitResponseDto = performMockDebitAccountAPICall(tokenResponseDto.getToken(), debitRequestDto);

        // then - verify the result content using assert statements
        assertEquals(
                balanceAfterDebitResponseDto.getBalance().stripTrailingZeros(),
                balanceAfterCreditResponseDto.getBalance().subtract(debitRequestDto.getAmount()).stripTrailingZeros()
        );
    }

    @Test
    public void scenario_registerUser_thenLoginWithRegisteredUser_thenGetJwtToken_thenCreditUsersAccount_thenDebitUsersAccount_thenGetUserBalance() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(userResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /register API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // step 5 - create CreditRequestDto
        CreditRequestDto creditRequestDto = new CreditRequestDto(new BigDecimal("985.24"), UUID.randomUUID().toString());

        // step 6 - call /api/account/credit API and get new account balance
        BalanceResponseDto balanceAfterCreditResponseDto = performMockCreditAccountAPICall(tokenResponseDto.getToken(), creditRequestDto);

        // step 7 - create DebitRequestDto
        DebitRequestDto debitRequestDto = new DebitRequestDto(new BigDecimal("512.65"), UUID.randomUUID().toString());

        // step 8 - call /api/account/debit API and get new account balance
        BalanceResponseDto balanceAfterDebitResponseDto = performMockDebitAccountAPICall(tokenResponseDto.getToken(), debitRequestDto);

        // step 9 - call /api/account/balance API and get new account balance
        BalanceResponseDto balanceResponseDto = performMockGetAccountBalanceAPICall(tokenResponseDto.getToken());

        // then - verify the result content using assert statements
        assertEquals(
                balanceResponseDto.getBalance().stripTrailingZeros(),
                balanceAfterCreditResponseDto.getBalance().subtract(debitRequestDto.getAmount()).stripTrailingZeros()
        );
        assertEquals(
                balanceAfterDebitResponseDto.getBalance().stripTrailingZeros(),
                balanceResponseDto.getBalance().stripTrailingZeros()
        );
    }

    @Test
    public void scenario_registerUser_thenLoginWithRegisteredUser_thenGetJwtToken_thenCreditUsersAccount_thenDebitUsersAccount_thenGetUserTransactionHistory() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(userResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /register API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // step 5 - create CreditRequestDto
        CreditRequestDto creditRequestDto = new CreditRequestDto(new BigDecimal("985.24"), UUID.randomUUID().toString());

        // step 6 - call /api/account/credit API and get new account balance
        BalanceResponseDto balanceAfterCreditResponseDto = performMockCreditAccountAPICall(tokenResponseDto.getToken(), creditRequestDto);

        // step 7 - create DebitRequestDto
        DebitRequestDto debitRequestDto = new DebitRequestDto(new BigDecimal("512.65"), UUID.randomUUID().toString());

        // step 8 - call /api/account/debit API and get new account balance
        BalanceResponseDto balanceAfterDebitResponseDto = performMockDebitAccountAPICall(tokenResponseDto.getToken(), debitRequestDto);

        // step 9 - call /api/account/history API and get transaction Histories
        ListResponseDto<TransactionResponseDto> transactionResponseDtoList = performMockGetAccountTransactionsHistoryAPICall(tokenResponseDto.getToken());

        // then - verify the result content using assert statements
        assertTrue(transactionResponseDtoList.getData().size() > 0);
        assertEquals(2, transactionResponseDtoList.getData().size());
        assertEquals(transactionResponseDtoList.getSize(), transactionResponseDtoList.getData().size());
        assertEquals(0, transactionResponseDtoList.getPage());
    }

}
