package com.leovegas.wallet_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.AccountRepository;
import com.leovegas.wallet_service.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author: s.shakeri
 * at 9/16/2022
 **/

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    @AfterEach
    void setup() {
        accountRepository.deleteAll();
        userRepository.deleteAll();
    }

    private UserResponseDto performMockRegisterUserAPICall(RegisterUserRequestDto registerUserRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerUserRequestDto)));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(registerUserRequestDto.getUsername())))
                .andExpect(jsonPath("$.role", is(Role.ROLE_USER.name())));

        // map response to dto and return
        MvcResult registerMvcResult = response.andReturn();
        String registerContentAsString = registerMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(registerContentAsString, UserResponseDto.class);
    }

    private TokenResponseDto performMockLoginAPICall(LoginRequestDto loginRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto)));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.token", is(notNullValue())));

        // map response to dto and return
        MvcResult loginMvcResult = response.andReturn();
        String loginContentAsString = loginMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(loginContentAsString, TokenResponseDto.class);
    }
    private UserResponseDto performMockGetUserInfoAPICall(TokenResponseDto tokenResponseDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(get("/api/user/info")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + tokenResponseDto.getToken()));

        // validate response
        response
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is(notNullValue())))
                .andExpect(jsonPath("$.role", is(notNullValue())));

        // map response to dto and return
        MvcResult getUserInfoMvcResult = response.andReturn();
        String getUserInfoContentAsString = getUserInfoMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(getUserInfoContentAsString, UserResponseDto.class);
    }

    @Test
    public void scenario_registerUser_thenLoginWithRegisteredUser_thenGetJwtToken_thenGetUserInfo() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /api/auth/register API and get created user
        UserResponseDto registerUserResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(registerUserResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /api/auth/login API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // step 5 - call /api/user/info API and get created user
        UserResponseDto userInfoResponseDto = performMockGetUserInfoAPICall(tokenResponseDto);

        // then - verify the result content using assert statements
        assertEquals(userInfoResponseDto.getUsername(), registerUserRequestDto.getUsername());
        assertEquals(userInfoResponseDto.getRole(), Role.ROLE_USER.name());
    }
}
