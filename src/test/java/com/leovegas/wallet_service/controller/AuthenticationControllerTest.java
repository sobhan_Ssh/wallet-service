package com.leovegas.wallet_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.AccountRepository;
import com.leovegas.wallet_service.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author: s.shakeri
 * at 9/16/2022
 **/

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    @AfterEach
    void setup() {
        accountRepository.deleteAll();
        userRepository.deleteAll();
    }

    public UserResponseDto performMockRegisterUserAPICall(RegisterUserRequestDto registerUserRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerUserRequestDto)));

        //validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(registerUserRequestDto.getUsername())))
                .andExpect(jsonPath("$.role", is(Role.ROLE_USER.name())));

        // map response to dto and return
        MvcResult registerMvcResult = response.andReturn();
        String registerContentAsString = registerMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(registerContentAsString, UserResponseDto.class);
    }

    private TokenResponseDto performMockLoginAPICall(LoginRequestDto loginRequestDto) throws Exception {

        // perform mock service call
        ResultActions response = mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto)));

        //validate response
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.token", is(notNullValue())));

        // map response to dto and return
        MvcResult loginMvcResult = response.andReturn();
        String loginContentAsString = loginMvcResult.getResponse().getContentAsString();
        return objectMapper.readValue(loginContentAsString, TokenResponseDto.class);
    }

    @Test
    public void scenario_registerNewUser_then_GetRegisteredUser() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // then - verify the result content using assert statements
        assertEquals(userResponseDto.getUsername(), registerUserRequestDto.getUsername());
        assertEquals(userResponseDto.getRole(), Role.ROLE_USER.name());
    }

    @Test
    public void scenario_registerUser_then_LoginWithRegisteredUser_then_GetJwtToken() throws Exception {
        // step 1 - create RegisterUserRequestDto
        RegisterUserRequestDto registerUserRequestDto = new RegisterUserRequestDto("Sobhan", "123456");

        // step 2 - call /register API and get created user
        UserResponseDto userResponseDto = performMockRegisterUserAPICall(registerUserRequestDto);

        // step 3 - create LoginRequestDto
        LoginRequestDto loginRequestDto = new LoginRequestDto(userResponseDto.getUsername(), registerUserRequestDto.getPassword());

        // step 4 - call /register API and get created user
        TokenResponseDto tokenResponseDto = performMockLoginAPICall(loginRequestDto);

        // then - verify the result content using assert statements
        assertNotNull(tokenResponseDto.getToken());
    }

}
