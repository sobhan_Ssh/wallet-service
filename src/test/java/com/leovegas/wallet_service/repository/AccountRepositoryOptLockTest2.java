package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AccountRepositoryOptLockTest2 {

    @Autowired
    private AccountRepository repository;
    private String savedAccountId;

    @BeforeEach
    public void insertUsers(){
        Account account = Account.builder()
                .balance(BigDecimal.ZERO)
                .build();
        savedAccountId = repository.save(account).getId();

    }

    @Test
    public void test_saving_account_optimistically() throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Account account = repository.findById(savedAccountId).orElse(null);
                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.setBalance(BigDecimal.ONE);
                try {
                    repository.save(account).getVersion();
                }catch (Exception e){
                    Assertions.assertEquals(e.getCause(),"StaleObjectStateException");
                }

            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Account account = repository.findById(savedAccountId).orElse(null);
                account.setBalance(BigDecimal.TEN);
                repository.save(account).getVersion();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }

}