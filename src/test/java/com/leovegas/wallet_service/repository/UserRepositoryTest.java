package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Author: s.shakeri
 * at 9/20/2022
 **/

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void should_find_no_user_if_repository_is_empty() {
        Iterable<User> users = userRepository.findAll();
        assertThat(users).isEmpty();
    }

    @Test
    public void should_save_a_user() {
        User user = new User("test", "123456", Role.ROLE_USER);
        userRepository.save(user);

        assertThat(user.getId()).isNotNull();
    }

    @Test
    public void should_find_all_users() {
        User user1 = new User("test", "123456", Role.ROLE_USER);
        User user2 = new User("test2", "123456", Role.ROLE_USER);
        userRepository.saveAll(Arrays.asList(user1, user2));

        Iterable<User> users = userRepository.findAll();

        assertThat(users).hasSize(2).contains(user1, user2);
    }

    @Test
    public void should_find_user_by_username() {
        User saveUser = new User("test", "123456", Role.ROLE_USER);
        userRepository.save(saveUser);

        User findUser = userRepository.findByUsername(saveUser.getUsername()).get();

        assertThat(findUser).isNotNull();
        assertThat(findUser).hasFieldOrPropertyWithValue("username", saveUser.getUsername());
    }
}
