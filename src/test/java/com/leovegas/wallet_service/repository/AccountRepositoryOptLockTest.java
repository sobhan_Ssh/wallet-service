package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.WalletServiceApplication;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = WalletServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountRepositoryOptLockTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Test
    void assert_object_optimistic_locking_failure() throws InterruptedException {
        // Create a user, so we can create account for user
        User user = new User("test", "123456", Role.ROLE_USER);
        userRepository.save(user);

        //create account for user
        Account account = new Account();
        account.setBalance(new BigDecimal("5000"));
        account.setUser(user);
        accountRepository.save(account);

        //CountDownLatch required for wait test to complete
        CountDownLatch countDownLatch = new CountDownLatch(2);

        // Create 2 AccountThread to perform update for exising account entity
        AccountThread account1 = new AccountThread(account.getBalance().add(new BigDecimal("1500")), account.getId(), countDownLatch);
        AccountThread account2 = new AccountThread(account.getBalance().subtract(new BigDecimal("1200")), account.getId(), countDownLatch);

        account1.start();
        account2.start();

        countDownLatch.await();

        // Validate one of thread have ObjectOptimisticLockingFailureException
        assertTrue(account1.hasObjectOptimisticLockingFailure() || account2.hasObjectOptimisticLockingFailure());
    }

    class AccountThread extends Thread {
        private final BigDecimal balance;
        private final String id;
        private final CountDownLatch countDownLatch;
        private Class exceptionClass;

        // verify exception
        boolean hasObjectOptimisticLockingFailure() {
            return this.exceptionClass == ObjectOptimisticLockingFailureException.class;
        }

        // Custom Thread class for performing account update and verify lock exception
        public AccountThread(BigDecimal balance, String id, CountDownLatch countDownLatch) {
            this.balance = balance;
            this.id = id;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            try {
                Account account = accountRepository.findById(id).get();
                account.setBalance(balance);
                accountRepository.save(account);
            } catch (Exception e) {
                exceptionClass = e.getClass();
            } finally {
                countDownLatch.countDown();
            }
        }
    }

}
