package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @Author: s.shakeri
 * at 9/20/2022
 **/

@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void should_find_no_accounts_if_repository_is_empty() {
        Iterable<Account> accounts = accountRepository.findAll();
        assertThat(accounts).isEmpty();
    }

    @Test
    public void should_save_an_Account() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account account = new Account();
        account.setBalance(new BigDecimal("0"));
        account.setUser(user);
        accountRepository.save(account);

        assertThat(account).hasFieldOrPropertyWithValue("balance", account.getBalance());
        assertThat(account).hasFieldOrPropertyWithValue("id", account.getId());
    }

    @Test
    public void should_find_all_accounts() {
        User user1 = new User("test", "123456", Role.ROLE_USER);
        User user2 = new User("test2", "123456", Role.ROLE_USER);
        userRepository.saveAll(Arrays.asList(user1, user2));

        Account account1 = new Account();
        account1.setBalance(new BigDecimal("1000"));
        account1.setUser(user1);

        Account account2 = new Account();
        account2.setBalance(new BigDecimal("2000"));
        account2.setUser(user2);

        accountRepository.saveAll(Arrays.asList(account1, account2));

        Iterable<Account> accounts = accountRepository.findAll();

        assertThat(accounts).hasSize(2).contains(account1, account2);
    }

    @Test
    public void should_find_account_by_username() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account saveAccount = new Account();
        saveAccount.setBalance(new BigDecimal("1500"));
        saveAccount.setUser(user);
        accountRepository.save(saveAccount);

        Account findAccount = new Account();
        findAccount = accountRepository.findAccountByUser_Username(user.getUsername()).get();

        assertThat(findAccount).hasFieldOrPropertyWithValue("balance", saveAccount.getBalance());
        assertThat(findAccount).hasFieldOrPropertyWithValue("id", saveAccount.getId());
    }
}
