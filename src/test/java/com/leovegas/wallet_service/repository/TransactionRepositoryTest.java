package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.Transaction;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.enums.TransactionType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Author: s.shakeri
 * at 9/21/2022
 **/

@DataJpaTest
public class TransactionRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void should_find_no_transactions_if_repository_is_empty() {
        Iterable<Transaction> transactions = transactionRepository.findAll();
        assertThat(transactions).isEmpty();
    }

    @Test
    public void should_save_a_transaction() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account account = new Account();
        account.setBalance(new BigDecimal("0"));
        account.setUser(user);
        accountRepository.save(account);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        transaction.setType(TransactionType.CREDIT);
        transaction.setAmount(new BigDecimal("500"));
        transaction.setAccount(account);
        transaction = transactionRepository.save(transaction);

        assertThat(transaction).hasFieldOrPropertyWithValue("id", transaction.getId());
        assertThat(transaction).hasFieldOrPropertyWithValue("amount", transaction.getAmount());
        assertThat(transaction).hasFieldOrPropertyWithValue("transactionId", transaction.getTransactionId());
        assertThat(transaction).hasFieldOrPropertyWithValue("type", transaction.getType());
    }

    @Test
    public void should_find_all_transactions() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account account = new Account();
        account.setBalance(new BigDecimal("0"));
        account.setUser(user);
        accountRepository.save(account);

        Transaction transaction1 = new Transaction();
        transaction1.setTransactionId(UUID.randomUUID().toString());
        transaction1.setType(TransactionType.CREDIT);
        transaction1.setAmount(new BigDecimal("500"));
        transaction1.setAccount(account);
        transactionRepository.save(transaction1);

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionId(UUID.randomUUID().toString());
        transaction2.setType(TransactionType.CREDIT);
        transaction2.setAmount(new BigDecimal("2500"));
        transaction2.setAccount(account);
        transactionRepository.save(transaction2);


        Iterable<Transaction> transactions = transactionRepository.findAll();

        assertThat(transactions).hasSize(2).contains(transaction1, transaction2);
    }

    @Test
    public void should_check_transaction_exist_by_transactionId() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account account = new Account();
        account.setBalance(new BigDecimal("0"));
        account.setUser(user);
        accountRepository.save(account);

        Transaction transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        transaction.setType(TransactionType.CREDIT);
        transaction.setAmount(new BigDecimal("500"));
        transaction.setAccount(account);
        transactionRepository.save(transaction);

        boolean exist = transactionRepository.existsByTransactionId(transaction.getTransactionId());

        assertThat(exist).isTrue();
    }

    @Test
    public void should_find_transactions_by_accountId() {
        User user = new User("test", "123456", Role.ROLE_USER);
        user = userRepository.save(user);

        Account account = new Account();
        account.setBalance(new BigDecimal("0"));
        account.setUser(user);
        accountRepository.save(account);

        Transaction transaction1 = new Transaction();
        transaction1.setTransactionId(UUID.randomUUID().toString());
        transaction1.setType(TransactionType.CREDIT);
        transaction1.setAmount(new BigDecimal("500"));
        transaction1.setAccount(account);
        transactionRepository.save(transaction1);

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionId(UUID.randomUUID().toString());
        transaction2.setType(TransactionType.CREDIT);
        transaction2.setAmount(new BigDecimal("2500"));
        transaction2.setAccount(account);
        transactionRepository.save(transaction2);

        Iterable<Transaction> transactions = transactionRepository.findTransactionsByAccount_Id(account.getId(), PageRequest.of(0, 10)).get().getContent();

        assertThat(transactions).hasSize(2).contains(transaction1, transaction2);
    }


}
