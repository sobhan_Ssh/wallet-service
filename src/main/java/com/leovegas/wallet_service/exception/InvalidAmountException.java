package com.leovegas.wallet_service.exception;

public class InvalidAmountException extends RuntimeException {

    public InvalidAmountException(String s) {
        super(s);
    }
}
