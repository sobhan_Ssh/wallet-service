package com.leovegas.wallet_service.exception;

public class InsufficientBalanceException extends RuntimeException {

    public InsufficientBalanceException(String s) {
        super(s);
    }
}