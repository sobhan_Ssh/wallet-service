package com.leovegas.wallet_service.exception;

public class DuplicateUsernameException extends RuntimeException {

    public DuplicateUsernameException(String s) {
        super(s);
    }
}