package com.leovegas.wallet_service.exception;

public class DuplicateTransactionIdException extends RuntimeException {

    public DuplicateTransactionIdException(String s) {
        super(s);
    }
}