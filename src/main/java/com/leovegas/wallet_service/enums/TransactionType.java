package com.leovegas.wallet_service.enums;

public enum TransactionType {

    DEBIT,
    CREDIT
}
