package com.leovegas.wallet_service.enums;

public enum Role {

    ROLE_ADMIN,
    ROLE_USER
}
