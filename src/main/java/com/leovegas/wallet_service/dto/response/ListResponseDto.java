package com.leovegas.wallet_service.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ListResponseDto<T> {

    private int page;
    private int size;
    private List<T> data = new ArrayList<>();

    public ListResponseDto(int page, int size) {
        this.page = page;
        this.size = size;
        this.data = new ArrayList<>();
    }

    public ListResponseDto(int page, int size, List<T> data) {
        this.page = page;
        this.size = size;
        this.data = data;
    }

    public ListResponseDto() {
    }

}
