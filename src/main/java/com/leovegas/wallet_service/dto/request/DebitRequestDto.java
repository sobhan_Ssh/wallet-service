package com.leovegas.wallet_service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DebitRequestDto extends BaseRequestDto {

    @NotNull(message = "amount is required")
    private BigDecimal amount;

    @NotEmpty(message = "transactionId is required")
    @NotNull(message = "transactionId is required")
    private String transactionId;

}
