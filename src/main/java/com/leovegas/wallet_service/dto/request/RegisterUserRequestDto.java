package com.leovegas.wallet_service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserRequestDto {

    @NotEmpty(message = "username is required")
    @NotNull(message = "username is required")
    private String username;

    @NotEmpty(message = "password is required")
    @NotNull(message = "password is required")
    private String password;

}
