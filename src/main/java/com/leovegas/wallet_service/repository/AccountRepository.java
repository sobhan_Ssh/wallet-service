package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

    Optional<Account> findAccountByUser_Username(String username);

}
