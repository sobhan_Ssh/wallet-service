package com.leovegas.wallet_service.repository;

import com.leovegas.wallet_service.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String> {

    boolean existsByTransactionId(String transactionId);

    Optional<Page<Transaction>> findTransactionsByAccount_Id(String accId, Pageable pageable);
}
