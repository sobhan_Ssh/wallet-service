package com.leovegas.wallet_service.controller;

import com.leovegas.wallet_service.dto.request.BaseRequestDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/info")
    public ResponseEntity<UserResponseDto> getUserDetails(@RequestHeader(name = "Authorization") Map<String, String> headers,
                                                          BaseRequestDto baseRequestDto) {
        return new ResponseEntity<>(userService.getUserByUsername(baseRequestDto.getUsername()), HttpStatus.OK);
    }
}
