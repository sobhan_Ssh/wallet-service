package com.leovegas.wallet_service.controller;

import com.leovegas.wallet_service.dto.request.BaseRequestDto;
import com.leovegas.wallet_service.dto.request.CreditRequestDto;
import com.leovegas.wallet_service.dto.request.DebitRequestDto;
import com.leovegas.wallet_service.dto.response.BalanceResponseDto;
import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.service.IAccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Validated
@RestController
@RequestMapping(path = "/api/account")
public class AccountController {

    private final IAccountService accountService;

    public AccountController(IAccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/debit")
    public ResponseEntity<BalanceResponseDto> debitAccount(@RequestHeader(name = "Authorization") Map<String, String> headers,
                                                           @Validated @RequestBody DebitRequestDto debitRequestDto) {
        return new ResponseEntity<>(accountService.debitAccount(debitRequestDto), HttpStatus.OK);
    }

    @PostMapping("/credit")
    public ResponseEntity<BalanceResponseDto> creditAccount(@RequestHeader(name = "Authorization") Map<String, String> headers,
                                                            @Validated @RequestBody CreditRequestDto creditRequestDto) {
        return new ResponseEntity<>(accountService.creditAccount(creditRequestDto), HttpStatus.OK);
    }

    @GetMapping("/balance")
    public ResponseEntity<BalanceResponseDto> getBalance(@RequestHeader(name = "Authorization") Map<String, String> headers,
                                                         BaseRequestDto baseRequestDto) {
        return new ResponseEntity<>(accountService.getBalance(baseRequestDto.getUsername()), HttpStatus.OK);
    }

    @GetMapping("/history")
    public ResponseEntity<ListResponseDto<TransactionResponseDto>> getAccountHistory(@RequestHeader(name = "Authorization") Map<String, String> headers,
                                                                                     @RequestParam(required = false, defaultValue = "0") int page,
                                                                                     @RequestParam(required = false, defaultValue = "20") int size,
                                                                                     BaseRequestDto baseRequestDto) {
        return new ResponseEntity<>(accountService.getAccountHistory(baseRequestDto.getUsername(), page, size), HttpStatus.OK);
    }
}
