package com.leovegas.wallet_service.controller;

import com.leovegas.wallet_service.annotation.AllowAnonymous;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.service.IAuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Validated
@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final IAuthenticationService authenticationService;

    public AuthenticationController(IAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;

    }

    @AllowAnonymous
    @PostMapping("/register")
    public ResponseEntity<UserResponseDto> registerUser(@Validated
                                                        @RequestBody RegisterUserRequestDto registerUserRequestDto) {
        return new ResponseEntity<>(authenticationService.register(registerUserRequestDto), HttpStatus.CREATED);
    }

    @AllowAnonymous
    @PostMapping("/login")
    public ResponseEntity<TokenResponseDto> loginUser(@Validated
                                                      @RequestBody LoginRequestDto loginRequestDto) {
        return new ResponseEntity<>(authenticationService.login(loginRequestDto), HttpStatus.CREATED);
    }
}
