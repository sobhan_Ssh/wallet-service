package com.leovegas.wallet_service.util;

public class Utility {

    public static boolean isNullOrEmpty(String data) {
        return data == null || data.trim().equals("");
    }

}
