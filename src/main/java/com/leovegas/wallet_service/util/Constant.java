package com.leovegas.wallet_service.util;

public class Constant {
    public static final String DEFAULT_UNEXPECTED_ERROR = "unexpected error";

    public static final String BAD_CREDENTIALS = "username and password does not match";
    public static final String ACCOUNT_NOT_FOUND = "no account found for the user, call support";
    public static final String INSUFFICIENT_BALANCE = "insufficient balance to debit, your balance is {0}";
    public static final String USER_NOT_FOUND = "could not find any user with provided token";
    public static final String INVALID_TOKEN = "invalid JWT Token";
    public static final String INVALID_TOKEN_FORMAT = "Invalid JWT Token in Bearer Header";
    public static final String DUPLICATE_TRANSACTION_ID = "duplicate transaction ID, try another one";
    public static final String DUPLICATE_USERNAME = "duplicate username, try another one";
    public static final String UNAUTHORIZED = "token is not provided or is invalid";
    public static final String INVALID_PRECISION_COUNT = "maximum valid count of digits for amount is {0}";
    public static final String INVALID_SCALE_COUNT = "maximum valid count of decimal point for amount is {0}";
    public static final String INVALID_NEGATIVE_AMOUNT = "amount can not be negative";


}
