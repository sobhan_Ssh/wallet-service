package com.leovegas.wallet_service.entity;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ACCOUNT")
@Where(clause = "status = 'ACTIVE'")
public class Account extends BaseEntity {

    private static final long serialVersionUID = 4995177431876476703L;

    @NotNull
    @Column(name = "BALANCE", precision = 25, scale = 5)
    private BigDecimal balance;

    @JoinColumn(name = "USER_ID")
    @OneToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    private User user;

    @Version
    private int version;

}
