package com.leovegas.wallet_service.entity;

import com.leovegas.wallet_service.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USERS")
@Where(clause = "status = 'ACTIVE'")
public class User extends BaseEntity {

    private static final long serialVersionUID = -3161435409982431891L;

    @NotNull
    @Column(name = "USERNAME", length = 50, unique = true)
    private String username;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @NotNull
    @Column(name = "ROLE", length = 20)
    @Enumerated(EnumType.STRING)
    private Role role;
}
