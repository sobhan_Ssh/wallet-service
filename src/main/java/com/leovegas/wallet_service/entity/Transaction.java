package com.leovegas.wallet_service.entity;

import com.leovegas.wallet_service.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TRANSACTIONS")
@Where(clause = "status = 'ACTIVE'")
public class Transaction extends BaseEntity {

    private static final long serialVersionUID = -2350643778585346828L;

    @NotNull
    @Column(name = "AMOUNT", precision = 25, scale = 5)
    private BigDecimal amount;

    @NotNull
    @Column(name = "TRANSACTION_ID", length = 50, unique = true)
    private String transactionId;

    @NotNull
    @Column(name = "TYPE", length = 20)
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ACCOUNT_ID", nullable = false)
    private Account account;
}
