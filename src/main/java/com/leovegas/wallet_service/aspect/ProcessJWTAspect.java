package com.leovegas.wallet_service.aspect;

import com.leovegas.wallet_service.dto.request.BaseRequestDto;
import com.leovegas.wallet_service.security.JWTUtil;
import com.leovegas.wallet_service.util.Utility;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.Map;

/**
 * @Author: s.shakeri
 * at 9/18/2022
 **/

@Aspect
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ProcessJWTAspect {

    private final JWTUtil jwtUtil;

    public ProcessJWTAspect(JWTUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void beanAnnotatedWithRestController() {
    }

    @Around("publicMethod() && beanAnnotatedWithRestController() && !@annotation(com.leovegas.wallet_service.annotation.AllowAnonymous)")
    public Object authorization(ProceedingJoinPoint pjp) throws Throwable {
        Object[] exArgs = pjp.getArgs();
        Map<String, String> headers;
        String jwt = "";

        headers = (Map<String, String>) exArgs[0];
        jwt = headers.get("Authorization");
        if (Utility.isNullOrEmpty(jwt))
            jwt = headers.get("authorization");
        jwt = jwt.substring(7);
        String username = jwtUtil.validateTokenAndRetrieveSubject(jwt);
        for (Object arg : pjp.getArgs()) {

            // Fill base request
            if (arg instanceof BaseRequestDto) {
                BaseRequestDto req = (BaseRequestDto) arg;
                req.setUsername(username);
            }
        }

        return pjp.proceed(exArgs);
    }
}
