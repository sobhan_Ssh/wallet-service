package com.leovegas.wallet_service.config;

import com.leovegas.wallet_service.exception.*;
import com.leovegas.wallet_service.util.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(Exception ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), Constant.DEFAULT_UNEXPECTED_ERROR);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidAmountException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidAmountException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(BadCredentialsException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(BadCredentialsException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), Constant.BAD_CREDENTIALS);
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(BadRequestException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(NotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InsufficientBalanceException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InsufficientBalanceException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(DuplicateTransactionIdException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(DuplicateTransactionIdException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }
    @ExceptionHandler(DuplicateUsernameException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(DuplicateUsernameException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }
}

