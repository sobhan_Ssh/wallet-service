package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.response.UserResponseDto;

public interface IUserService {

    UserResponseDto getUserByUsername(String username);

}
