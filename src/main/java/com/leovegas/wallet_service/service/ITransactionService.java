package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.Transaction;
import com.leovegas.wallet_service.enums.TransactionType;

import java.math.BigDecimal;

public interface ITransactionService {

    Transaction createTransaction(Account account, BigDecimal amount, String transactionId, TransactionType transactionType);

    ListResponseDto<TransactionResponseDto> getAccountTransactions(Account account, int page, int size);
}
