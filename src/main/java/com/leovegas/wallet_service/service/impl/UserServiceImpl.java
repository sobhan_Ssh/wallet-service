package com.leovegas.wallet_service.service.impl;

import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.repository.UserRepository;
import com.leovegas.wallet_service.service.IUserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserResponseDto getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        return user
                .map(value -> new UserResponseDto(value.getUsername(), value.getRole().name()))
                .orElse(new UserResponseDto());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> tempUser = userRepository.findByUsername(username);
        if (tempUser.isPresent()) {
            User user = tempUser.get();
            return new org.springframework.security.core.userdetails.User(
                    username,
                    user.getPassword(),
                    Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        } else
            throw new UsernameNotFoundException("Could not findUser with username = " + username);
    }
}
