package com.leovegas.wallet_service.service.impl;

import com.leovegas.wallet_service.dto.request.CreditRequestDto;
import com.leovegas.wallet_service.dto.request.DebitRequestDto;
import com.leovegas.wallet_service.dto.response.BalanceResponseDto;
import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.TransactionType;
import com.leovegas.wallet_service.exception.InsufficientBalanceException;
import com.leovegas.wallet_service.exception.InvalidAmountException;
import com.leovegas.wallet_service.exception.NotFoundException;
import com.leovegas.wallet_service.repository.AccountRepository;
import com.leovegas.wallet_service.service.IAccountService;
import com.leovegas.wallet_service.service.ITransactionService;
import com.leovegas.wallet_service.util.Constant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.MessageFormat;

@Service
public class AccountServiceImpl implements IAccountService {

    private final static int maxPrecisionCount = 25;
    private final static int maxScaleCount = 5;

    private final AccountRepository repository;
    private final ITransactionService transactionService;

    public AccountServiceImpl(AccountRepository repository, ITransactionService transactionService) {
        this.repository = repository;
        this.transactionService = transactionService;
    }

    private Account findAccountByUsername(String username) {
        return repository
                .findAccountByUser_Username(username)
                .orElseThrow(() -> new NotFoundException(Constant.ACCOUNT_NOT_FOUND));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Account createAccountForUser(User user) {
        Account account = new Account();
        account.setUser(user);
        account.setBalance(new BigDecimal("0"));
        return repository.save(account);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized BalanceResponseDto debitAccount(DebitRequestDto debitRequestDto) {
        validateAmountsPrecisionAndScale(debitRequestDto.getAmount());
        Account account = findAccountByUsername(debitRequestDto.getUsername());
        if (account.getBalance().compareTo(debitRequestDto.getAmount()) >= 0) {
            transactionService.createTransaction(account, debitRequestDto.getAmount(),
                    debitRequestDto.getTransactionId(), TransactionType.DEBIT);
            account.setBalance(account.getBalance().subtract(debitRequestDto.getAmount()));
            account = repository.save(account);
            return new BalanceResponseDto(account.getBalance());
        } else
            throw new InsufficientBalanceException(MessageFormat.format(Constant.INSUFFICIENT_BALANCE, account.getBalance()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized BalanceResponseDto creditAccount(CreditRequestDto creditRequestDto) {
        validateAmountsPrecisionAndScale(creditRequestDto.getAmount());
        Account account = findAccountByUsername(creditRequestDto.getUsername());
        transactionService.createTransaction(account, creditRequestDto.getAmount(),
                creditRequestDto.getTransactionId(), TransactionType.CREDIT);
        account.setBalance(account.getBalance().add(creditRequestDto.getAmount()));
        account = repository.save(account);
        return new BalanceResponseDto(account.getBalance());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BalanceResponseDto getBalance(String username) {
        Account account = findAccountByUsername(username);
        return new BalanceResponseDto(account.getBalance());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ListResponseDto<TransactionResponseDto> getAccountHistory(String username, int page, int size) {
        Account account = findAccountByUsername(username);
        return transactionService.getAccountTransactions(account, page, size);
    }

    private void validateAmountsPrecisionAndScale(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            if (!(amount.precision() > maxPrecisionCount)) {
                if (amount.scale() > maxScaleCount)
                    throw new InvalidAmountException(MessageFormat.format(Constant.INVALID_SCALE_COUNT, maxScaleCount));
            } else
                throw new InvalidAmountException(MessageFormat.format(Constant.INVALID_PRECISION_COUNT, maxPrecisionCount));
        } else
            throw new InvalidAmountException(Constant.INVALID_NEGATIVE_AMOUNT);
    }
}
