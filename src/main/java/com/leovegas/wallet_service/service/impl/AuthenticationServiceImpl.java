package com.leovegas.wallet_service.service.impl;

import com.leovegas.wallet_service.exception.DuplicateUsernameException;
import com.leovegas.wallet_service.security.JWTUtil;
import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;
import com.leovegas.wallet_service.entity.User;
import com.leovegas.wallet_service.enums.Role;
import com.leovegas.wallet_service.repository.UserRepository;
import com.leovegas.wallet_service.service.IAccountService;
import com.leovegas.wallet_service.service.IAuthenticationService;
import com.leovegas.wallet_service.util.Constant;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final JWTUtil jwtUtil;
    private final AuthenticationManager authManager;

    private final IAccountService accountService;

    public AuthenticationServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository, JWTUtil jwtUtil, AuthenticationManager authManager, IAccountService accountService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
        this.authManager = authManager;
        this.accountService = accountService;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserResponseDto register(RegisterUserRequestDto registerUserRequestDto) {
        if (!userRepository.existsUserByUsername(registerUserRequestDto.getUsername())) {
            String encodedPass = passwordEncoder.encode(registerUserRequestDto.getPassword());
            registerUserRequestDto.setPassword(encodedPass);
            User user = new User(registerUserRequestDto.getUsername(), registerUserRequestDto.getPassword(), Role.ROLE_USER);
            user = userRepository.save(user);
            accountService.createAccountForUser(user);
            return new UserResponseDto(user.getUsername(), user.getRole().name());
        } else
            throw new DuplicateUsernameException(Constant.DUPLICATE_USERNAME);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TokenResponseDto login(LoginRequestDto loginRequestDto) {
        UsernamePasswordAuthenticationToken authInputToken = new
                UsernamePasswordAuthenticationToken(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        authManager.authenticate(authInputToken);
        String token = jwtUtil.generateToken(loginRequestDto.getUsername());
        return new TokenResponseDto(token);
    }
}
