package com.leovegas.wallet_service.service.impl;

import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.Transaction;
import com.leovegas.wallet_service.enums.TransactionType;
import com.leovegas.wallet_service.exception.DuplicateTransactionIdException;
import com.leovegas.wallet_service.repository.TransactionRepository;
import com.leovegas.wallet_service.service.ITransactionService;
import com.leovegas.wallet_service.util.Constant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl implements ITransactionService {

    private final TransactionRepository repository;

    public TransactionServiceImpl(TransactionRepository repository) {
        this.repository = repository;
    }

    private void checkTransactionIdUniqueness(String transactionId) {
        if (repository.existsByTransactionId(transactionId))
            throw new DuplicateTransactionIdException(Constant.DUPLICATE_TRANSACTION_ID);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Transaction createTransaction(Account account, BigDecimal amount, String transactionId, TransactionType type) {
        checkTransactionIdUniqueness(transactionId);
        Transaction transaction = new Transaction(amount, transactionId, type, account);
        return repository.save(transaction);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ListResponseDto<TransactionResponseDto> getAccountTransactions(Account account, int page, int size) {
        ListResponseDto<TransactionResponseDto> responseDto = new ListResponseDto<>();
        Optional<Page<Transaction>> temp =
                repository.findTransactionsByAccount_Id(account.getId(), PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdTime")));
        if (temp.isPresent() && !CollectionUtils.isEmpty(temp.get().getContent())) {
            List<TransactionResponseDto> responseData = mapTransactionsToTransactionResponseDto(temp.get().getContent());
            responseDto = new ListResponseDto<>(page, responseData.size(), responseData);
        }
        return responseDto;
    }
    private List<TransactionResponseDto> mapTransactionsToTransactionResponseDto(List<Transaction> transactionList) {
        List<TransactionResponseDto> transactionResponseList = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
            transactionResponseDto.setTransactionId(transaction.getTransactionId());
            transactionResponseDto.setAmount(transaction.getAmount());
            transactionResponseDto.setCreatedTime(transaction.getCreatedTime());
            transactionResponseDto.setType(transaction.getType().name());
            transactionResponseList.add(transactionResponseDto);
        }
        return transactionResponseList;
    }
}
