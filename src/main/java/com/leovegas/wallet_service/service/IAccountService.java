package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.request.CreditRequestDto;
import com.leovegas.wallet_service.dto.request.DebitRequestDto;
import com.leovegas.wallet_service.dto.response.BalanceResponseDto;
import com.leovegas.wallet_service.dto.response.ListResponseDto;
import com.leovegas.wallet_service.dto.response.TransactionResponseDto;
import com.leovegas.wallet_service.entity.Account;
import com.leovegas.wallet_service.entity.User;

public interface IAccountService {

    Account createAccountForUser(User user);
    BalanceResponseDto debitAccount(DebitRequestDto debitRequestDto);
    BalanceResponseDto creditAccount(CreditRequestDto creditRequestDto);
    BalanceResponseDto getBalance(String username);
    ListResponseDto<TransactionResponseDto> getAccountHistory(String username, int page, int size);
}
