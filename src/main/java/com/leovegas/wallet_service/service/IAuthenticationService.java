package com.leovegas.wallet_service.service;

import com.leovegas.wallet_service.dto.request.LoginRequestDto;
import com.leovegas.wallet_service.dto.request.RegisterUserRequestDto;
import com.leovegas.wallet_service.dto.response.TokenResponseDto;
import com.leovegas.wallet_service.dto.response.UserResponseDto;

public interface IAuthenticationService {

    UserResponseDto register(RegisterUserRequestDto registerUserRequestDto);

    TokenResponseDto login(LoginRequestDto loginRequestDto);
}
